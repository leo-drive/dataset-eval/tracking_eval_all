from waymo_open_dataset.utils import range_image_utils
from waymo_open_dataset.utils import transform_utils
from waymo_open_dataset.utils import frame_utils
from waymo_open_dataset import dataset_pb2 as open_dataset
import tensorflow.compat.v1 as tf
from waymo_open_dataset.protos import metrics_pb2
from waymo_open_dataset import label_pb2

class Helper(object):
    def __init__(self, path_tfrecord):
        self.path_tfrecord = path_tfrecord
        self.segment_name = self.path_tfrecord.split('/')[-1].split('-')[-1].split('.')[0].split('_with')[0]
        self.list_types = [1, 2, 4]
        # Proto objects all:
        self.proto_objects_calculated = metrics_pb2.Objects()
        self.output_path = str('/home/goktug/Desktop/pred_autoware.bin')
        self.f = open(self.output_path, 'ab+')
        self.readDataset()

    def readDataset(self):
        dataset = tf.data.TFRecordDataset(self.path_tfrecord, compression_type='')
        list_frame_ids = []
        for frame_id, data in enumerate(dataset):
            frame = open_dataset.Frame()
            frame.ParseFromString(bytearray(data.numpy()))
            for index, laser_label in enumerate(frame.laser_labels):
                frame_stamp_secs = int(frame.timestamp_micros / 1000000)
                frame_stamp_nsecs = int((frame.timestamp_micros / 1000000.0 - frame_stamp_secs) * 1000000000)
                if laser_label.type not in self.list_types:
                    continue

                object_calculated = metrics_pb2.Object()
                object_calculated.context_name = self.segment_name
                object_calculated.frame_timestamp_micros = frame.timestamp_micros

                box = label_pb2.Label.Box()
                box.center_x = laser_label.box.center_x + 0.05
                box.center_y = laser_label.box.center_y + 0.05
                box.center_z = laser_label.box.center_z + 0.05
                box.length = laser_label.box.length
                box.width = laser_label.box.width
                box.height = laser_label.box.height
                box.heading = laser_label.box.heading
                object_calculated.object.box.CopyFrom(box)

                object_calculated.score = 1.0

                object_calculated.object.id = laser_label.id

                object_calculated.object.type = laser_label.type

                self.proto_objects_calculated.objects.append(object_calculated)

        self.f.write(self.proto_objects_calculated.SerializeToString())
        self.f.close()
        print("Prediction file is generated:", self.output_path)


if __name__ == '__main__':
    path_segment = 'detection_3d_vehicle_detection_test.bin'
    path = '/home/goktug/Downloads/' + path_segment
    helper = Helper(path)
