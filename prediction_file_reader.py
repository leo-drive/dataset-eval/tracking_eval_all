import sys
import os
import tensorflow.compat.v1 as tf
import math
import numpy as np
import itertools
from waymo_open_dataset import dataset_pb2
from waymo_open_dataset import label_pb2
from waymo_open_dataset.protos import metrics_pb2
tf.enable_eager_execution()
from waymo_open_dataset.utils import range_image_utils
from waymo_open_dataset.utils import transform_utils
from waymo_open_dataset.utils import frame_utils
from waymo_open_dataset import dataset_pb2 as open_dataset
import random

def list_context_names(path_predcition_file):
    list_set_context_names = []
    f = open(path_predcition_file, 'rb')
    objects = metrics_pb2.Objects()
    objects.ParseFromString(f.read())
    for object in objects.objects:
        if object.context_name not in list_set_context_names:
            list_set_context_names.append(object.context_name)
    print(list_set_context_names)

    f.close()
    return list_set_context_names


if __name__ == '__main__':
    list_set_context_names = list_context_names(sys.argv[1])
    if(len(sys.argv) == 3):
        segment_name = sys.argv[2]
        context_name = segment_name.split('_with')[0].split('-')[1]
        if context_name in list_set_context_names:
            print("TRUE")
        else:
            print("FALSE")

