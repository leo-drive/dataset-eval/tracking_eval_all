import rclpy
from rclpy.node import Node
from helper import Helper
from helper import Visualizer

# Alternative for tf py
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
import numpy as np
from autoware_auto_msgs.msg import TrackedObject
from autoware_auto_msgs.msg import TrackedObjects
from waymo_open_dataset.protos import metrics_pb2
from waymo_open_dataset import label_pb2
import sys


class DetectionPublisherNode(Node):
    def __init__(self):
        super().__init__('waymo_prediction_generator_node')
        path_tf_record = "/home/goktug/projects/tracking_eval_all/tfrecords/"
        segment_name = sys.argv[1]
        print(segment_name)
        path_tf_record = path_tf_record + segment_name
        self.helper = Helper(path_tf_record)
        self.helper.read_tfrecord()
        # Subscribe tracked objects:
        self.subscription = self.create_subscription(TrackedObjects, '/tracked_objects', self.CallbackTrackedObjects,
                                                     10)
        # Publis MarkerArr:
        self.pub_markerArr = self.create_publisher(MarkerArray, "/markerArray_tracked_object_ids", 0)
        # Proto objects all:
        self.proto_objects_calculated = metrics_pb2.Objects()
        self.output_path = str('/home/goktug/Desktop/pred_autoware.bin')
        self.f = open(self.output_path, 'ab+')


    def CallbackTrackedObjects(self, tracked_objects):

        frame_id, timestamp_waymo, context_name, total_frame_count = self.helper.get_dataset_timestamp(
            tracked_objects.header.stamp.sec,
            tracked_objects.header.stamp.nanosec)
        print("Tracked objects are received. Frame id:", frame_id)
        print("Tracked object count", len(tracked_objects.objects))

        markerArr = MarkerArray()



        for tracked_object in tracked_objects.objects:
            object_calculated = metrics_pb2.Object()
            object_calculated.context_name = context_name
            object_calculated.frame_timestamp_micros = timestamp_waymo

            best_z = self.helper.get_closest_object(frame_id, tracked_object)

            center_x, center_y, center_z, heading, height, width, length, object_id, type \
                = self.helper.give_tracked_object_info(tracked_object)

            center_z = best_z

            # Visualize:
            visualizer = Visualizer(center_x, center_y, center_z, heading, height, width, length, object_id,
                                    type, tracked_objects.header.stamp.sec,
                                    tracked_objects.header.stamp.nanosec)
            cube_marker = visualizer.give_cube_marker()
            arrow_marker = visualizer.give_arrow_marker()
            id_marker = visualizer.give_id_marker()
            center_marker = visualizer.giveMarkerPoints()
            class_name_marker = visualizer.give_marker_class_name()
            markerArr.markers.append(cube_marker)
            markerArr.markers.append(arrow_marker)
            markerArr.markers.append(id_marker)
            markerArr.markers.append(center_marker)
            markerArr.markers.append(class_name_marker)

            box = label_pb2.Label.Box()
            box.center_x = center_x
            box.center_y = center_y
            box.center_z = center_z
            box.length = length
            box.width = width
            box.height = height
            box.heading = heading
            object_calculated.object.box.CopyFrom(box)

            object_calculated.score = 1.0

            object_calculated.object.id = object_id

            object_calculated.object.type = type

            self.proto_objects_calculated.objects.append(object_calculated)

        self.pub_markerArr.publish(markerArr)
        print("***************************************")

        if frame_id == total_frame_count - 1:
            self.f.write(self.proto_objects_calculated.SerializeToString())
            self.f.close()
            print("Prediction file is generated:", self.output_path)



def main(args=None):
    rclpy.init(args=args)
    node_obj = DetectionPublisherNode()
    rclpy.spin(node_obj)
    node_obj.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
